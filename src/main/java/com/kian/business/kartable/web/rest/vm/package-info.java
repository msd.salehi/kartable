/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kian.business.kartable.web.rest.vm;
